package miaouprod.ecrire_grave;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;



public class App 
{
	
	private static final String REPGRAV = "C:\\Users\\tangi\\Desktop\\comptage";
	
	
	public static void main(String[] args) throws IOException {
		
			Integer maxFolder = Files
					// Ouverture d'un stream
			.list(Paths.get(REPGRAV))
					// On ne concerve que les répertoires
			.filter (Files::isDirectory)
					// flux de path (une forme de représentation de données en java) que l'on transforme en chaine de caractères
			.map(path->path.getFileName().toString())
					// on filtre pour ne garderque les répertoire qui ont un nom contenant un "."
			.filter(folderName -> folderName.contains("."))
					// on découpe alors le nom pour ne garder que la partie avant le "."
			.map(folderName -> folderName.split("\\.")[0])
					// on test s'il n'y a bien que des chiffres, on ne sait jamais
			.filter(folderNumber -> folderNumber.matches("[0 -9]+"))
					// On tranforme le tout en entiers
			.mapToInt(Integer::parseInt)
					// On garde la valeur Max
			.max()
					//Si aucune réponse n'est retournée (répertoire vide ou qui ne contient pas de répertoire avec des noms contenant un point par exemple) je retourne -1 comme ça quand je ferai +1 ça me fera 0.
			.orElse(-1);
		
			System.out.println(maxFolder);
	}
}
